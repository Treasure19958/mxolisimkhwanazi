package za.co.four.assignment.MxolisiMkhwanazi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "routes")
public class Route {

    @Id
    @Column(name = "routeId")
    private int routeId;

    @Column(name = "planetOrigin")
    private String planetOrigin;

    @Column(name = "planetDestination")
    private String planetDestination;

    @Column(name = "distance")
    private double distance;

    public Route(int routeId, String planetOrigin, String planetDestination, double distance) {
        this.routeId = routeId;
        this.planetOrigin = planetOrigin;
        this.planetDestination = planetDestination;
        this.distance = distance;
    }

    public Route() {

    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getPlanetOrigin() {
        return planetOrigin;
    }

    public void setPlanetOrigin(String planetOrigin) {
        this.planetOrigin = planetOrigin;
    }

    public String getPlanetDestination() {
        return planetDestination;
    }

    public void setPlanetDestination(String planetDestination) {
        this.planetDestination = planetDestination;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeId=" + routeId +
                ", planetOrigin='" + planetOrigin + '\'' +
                ", planetDestination='" + planetDestination + '\'' +
                ", distance=" + distance +
                '}';
    }

}
