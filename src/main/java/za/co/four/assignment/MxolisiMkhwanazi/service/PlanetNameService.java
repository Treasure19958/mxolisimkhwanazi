package za.co.four.assignment.MxolisiMkhwanazi.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;


public interface PlanetNameService {

    void savePlanetName(MultipartFile file);
    List<PlanetName> findAllPlanetNames();
}
