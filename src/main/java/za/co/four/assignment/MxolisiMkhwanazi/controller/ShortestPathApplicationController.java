package za.co.four.assignment.MxolisiMkhwanazi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.ExcelHelper.ExcelFileReaderHelper;
import za.co.four.assignment.MxolisiMkhwanazi.message.response.ResponseMessage;
import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;
import za.co.four.assignment.MxolisiMkhwanazi.model.Route;
import za.co.four.assignment.MxolisiMkhwanazi.service.PlanetNameService;
import za.co.four.assignment.MxolisiMkhwanazi.service.RouteService;
import za.co.four.assignment.MxolisiMkhwanazi.service.ShortestPathService;

@CrossOrigin("*")
@Controller
@RequestMapping("/api")
public class ShortestPathApplicationController {

    @Autowired
    PlanetNameService planetNameService;

    @Autowired
    RouteService routeService;

    @Autowired
    ShortestPathService shortestPathService;

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (ExcelFileReaderHelper.hasExcelFormat(file)) {
            try {
                planetNameService.savePlanetName(file);
                routeService.saveRoue(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    @GetMapping("/planetnames")
    public ResponseEntity<List<PlanetName>> getAllPlanetNames() {
        try {
            List<PlanetName> planetNames = planetNameService.findAllPlanetNames();

            if (planetNames.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(planetNames, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/routes")
    public ResponseEntity<List<Route>> getAllRoutes() {
        try {
            List<Route> routes = routeService.findAllRoutes();

            if (routes.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(routes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/shortestpath/{planetOrigin}/{planetDest}")
    public ResponseEntity<List<String>> getShortestPath(@PathVariable String planetOrigin, @PathVariable String planetDest){
        try {
            List<String> paths = shortestPathService.getPath(planetOrigin, planetDest);

            if (paths.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(paths, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
