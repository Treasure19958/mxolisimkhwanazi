package za.co.four.assignment.MxolisiMkhwanazi.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import za.co.four.assignment.MxolisiMkhwanazi.algorithm.DeterminePath;
import za.co.four.assignment.MxolisiMkhwanazi.model.Graph;
import za.co.four.assignment.MxolisiMkhwanazi.model.PlanetName;
import za.co.four.assignment.MxolisiMkhwanazi.model.Route;
import za.co.four.assignment.MxolisiMkhwanazi.service.PlanetNameService;
import za.co.four.assignment.MxolisiMkhwanazi.service.RouteService;
import za.co.four.assignment.MxolisiMkhwanazi.service.ShortestPathService;

@Service
public class ShortestPathServiceImpl implements ShortestPathService {

    @Autowired
    PlanetNameService planetNameService;

    @Autowired
    RouteService routeService;

    private List<PlanetName> planetNames;
    private List<Route> routes;
    private List<PlanetName> nodes = new ArrayList<PlanetName>();
    private List<Route> edges = new ArrayList<Route>();

    public void execute(){
        planetNames = planetNameService.findAllPlanetNames();
        routes = routeService.findAllRoutes();

        for(int i = 0; i < planetNames.size(); i++) {

            PlanetName planetName = new PlanetName(planetNames.get(i).getPlanetNode(), planetNames.get(i).getPlanetName());
            nodes.add(planetName);
        }

        addEdges(routes, edges);

    }

    private void addEdges(List<Route> routes, List<Route> edges) {

         for(int i = 0; i < routes.size(); i++) {

             Route route = new Route(routes.get(i).getRouteId(), routes.get(i).getPlanetOrigin(),
                     routes.get(i).getPlanetDestination(), routes.get(i).getDistance());
             edges.add(route);
         }

    }

    @Override
    public List<String> getPath(String planetOrigin, String planetDest) {
        execute();
        Graph graph = new Graph(nodes, edges);
        DeterminePath determinePath = new DeterminePath(graph);
        determinePath.execute(planetOrigin);
        LinkedList<String> path = determinePath.getPath(planetDest);
        return getShortestPath(path);
    }

    private List<String> getShortestPath(LinkedList<String> path) {
        List<String> stringList = new ArrayList<String>();
        for(String data: path){
             stringList.add(data);
        }
        return stringList;
    }
}
