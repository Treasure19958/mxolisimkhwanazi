package za.co.four.assignment.MxolisiMkhwanazi.model;

import java.util.List;

public class Graph {

    private List<PlanetName> planetNames;
    private List<Route> routes;

    public Graph(List<PlanetName> planetNames, List<Route> routes) {
        this.planetNames = planetNames;
        this.routes = routes;
    }

    public List<PlanetName> getPlanetNames() {
        return planetNames;
    }

    public List<Route> getRoutes() {
        return routes;
    }
}
