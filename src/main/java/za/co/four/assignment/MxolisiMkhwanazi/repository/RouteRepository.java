package za.co.four.assignment.MxolisiMkhwanazi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import za.co.four.assignment.MxolisiMkhwanazi.model.Route;

public interface RouteRepository extends JpaRepository<Route, Integer> {

}
