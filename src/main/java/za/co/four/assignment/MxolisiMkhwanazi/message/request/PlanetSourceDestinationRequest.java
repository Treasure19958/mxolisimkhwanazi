package za.co.four.assignment.MxolisiMkhwanazi.message.request;

public class PlanetSourceDestinationRequest {

    private String planetOrgin;
    private String planetDest;

    public PlanetSourceDestinationRequest(String planetOrgin, String planetDest) {
        this.planetOrgin = planetOrgin;
        this.planetDest = planetDest;
    }

    public String getPlanetOrgin() {
        return planetOrgin;
    }

    public void setPlanetOrgin(String planetOrgin) {
        this.planetOrgin = planetOrgin;
    }

    public String getPlanetDest() {
        return planetDest;
    }

    public void setPlanetDest(String planetDest) {
        this.planetDest = planetDest;
    }
}
